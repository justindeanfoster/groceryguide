import React from 'react';
import 'antd/dist/antd.css';
import './styles/home.css'
import { Typography, Carousel, Card } from 'antd';
import { Button, Row, Col } from 'antd';
import '../components/styles/card.css'

import { Link } from 'react-router-dom'

const { Title } = Typography;
const { Meta } = Card


class Home extends React.Component {

    render() {
        return <>
            <Carousel dotPosition='top' autoplay>
                <div>
                    <div className='cover-image-1'/>
                </div>
                <div>
                    <div className='cover-image-2'/>
                </div>
                <div>
                    <div className='cover-image-3'/>
                </div>
            </Carousel>
            <div className='centered-card'>

                <Card 
                className='introcard'
                >
                    <Typography>
                        <Title> Welcome to <b className='bolded'>Grocery</b>Guide! </Title>
                    </Typography>
                    <Meta
                    description={`
                        GroceryGuide helps you make informed decisions about your next shopping trip by 
                        compiling information from local stores and nutritional databases. As students of the 
                        University of Texas - Austin, food deserts are a big deal to us! Healthy
                        and cheap food should be accessible to everyone. Creating this website is our first step 
                        in helping everyone find reasonably priced and nutritious groceries.
                    `}
                    >
                    </Meta>

                    <br/>

                    <Row justify='center' gutter='16'>
                        <Col>
                            <Link to='/locations'>
                                <Button type='primary'> Explore Cities </Button>
                            </Link>
                        </Col>
                        <Col>
                            <Link to='/items'>
                                <Button type='primary'> Explore Items </Button>
                            </Link>
                        </Col>
                        <Col>
                            <Link to='/stores'>
                                <Button type='primary'> Explore Stores </Button>
                            </Link>
                        </Col>
                    </Row>

                </Card>
            </div>
      </>
    }
}
export default Home;
