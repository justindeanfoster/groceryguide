import React from 'react';
import 'antd/dist/antd.css';
import './styles/stores.css'
import StoreCard from '../components/StoreCard';
import { PageHeader, Row, Col, Descriptions, Input, Popover } from 'antd';
const { Search } = Input

// Do nothing on search for now.
const onSearch = value => console.log(value);

// Values for 3 stores.
const properties = [
    {
        storeName: 'Walmart',
        storeImage: 'https://h7f7z2r7.stackpathcdn.com/sites/default/files/images/articles/walmart2.jpg',
        storeAvatar: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Walmart_Spark.svg/1925px-Walmart_Spark.svg.png',
        linkTo: '/stores/walmart',
    },
    {
        storeName: 'Target',
        storeImage: 'https://corporate.target.com/_media/TargetCorp/about/images/stores/Upcoming_stores_hero.jpg?ext=.jpg',
        storeAvatar: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Target_logo.svg/1541px-Target_logo.svg.png',
        linkTo: '/stores/target'
    },
    {
        storeName: 'H-E-B',
        storeImage: 'https://www.kxan.com/wp-content/uploads/sites/40/2020/03/heb-1.jpg?w=2560&h=1440&crop=1',
        storeAvatar: 'https://thetrailfoundation.org/wp-content/uploads/2014/03/HEB-logo.jpg',
        linkTo: '/stores/heb'
    }
]

const storeCards = properties.map( prop => <StoreCard {...prop}/>)

class Store extends React.Component {
    handleVisibleChange = visible => {
        this.setState({ visible });
    };

    render() {
        return (
            <>
                <div className='page-header-wrapper'>
                    <PageHeader
                    ghost={false}
                    title='Available Stores'
                    subTitle='Browse/search for available stores'
                    >
                        <Descriptions size='small' column={3}>
                            <Descriptions.Item label='Last Updated'>10/2/2021</Descriptions.Item>
                        </Descriptions>
                        <Popover
                            title="Search is still a work in progress!"
                            trigger="click"
                        >
                            <Search
                            placeholder='H-E-B'
                            allowClear
                            enterButton='Search'
                            size='large'
                            onSearch={onSearch}
                            />
                        </Popover>
                        <Descriptions size='small' column={3}>
                            <Descriptions.Item label='Number of Instances'>3</Descriptions.Item>
                        </Descriptions>
                        <Descriptions size='small' column={3}>
                            <Descriptions.Item label='Number of Pages'>1</Descriptions.Item>
                        </Descriptions>
                    </PageHeader>
                </div>
    
    
                <div className="page-header-wrapper">
                    <Row gutter={20}>
                        <Col span={8}>
                            { storeCards[0] }
                        </Col>
                        <Col span={8}>
                            { storeCards[1] }
                        </Col>
                        <Col span={8}>
                            { storeCards[2] }
                        </Col>
                    </Row>
                </div>
            </>
        );
    }
}

export default Store