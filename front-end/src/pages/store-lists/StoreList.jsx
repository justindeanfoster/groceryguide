import React from 'react';
import './storeList.css'
import { PageHeader, Table, Tag, Button, Descriptions } from 'antd'
import { Link } from 'react-router-dom'


const ButtonAustin = () => {
    return (
        <Link to='/locations/Austin'>
            <Button type='secondary'> See city </Button>
        </Link>
    )
}

const ButtonDallas = () => {
    return (
        <Link to='/locations/Dallas'>
            <Button type='secondary'> See city </Button>
        </Link>
    )
}

const ButtonSanAntonio = () => {
    return (
        <Link to='/locations/SanAntonio'>
            <Button type='secondary'> See city </Button>
        </Link>
    )
}

const columns = [
    {
        title: 'Store',
        dataIndex: 'num',
        key: 'num',
        render: num => {
            let url = `/stores/walmart/${num}`
            return <Link to={url}>
                <Button type='primary'> Go to store </Button>
            </Link>
        }
    },
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name'
    },
    {
        title: 'City',
        dataIndex: 'city',
        key: 'city',
    },
    {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        render: price => {

            let color = 'red'
            if (price === '$') {
                color = 'green'
            } else if (price === '$$') {
                color = 'gold'
            } else if (price == null) {
                color = 'purple'
            }

            return (
                <Tag color={color} key={price}>
                    { price == null ? '?' : '$'}
                </Tag>
            )

        }
    },
    {
        title: 'Open',
        dataIndex: 'open',
        key: 'open'
    },
    {
        title: 'Rating',
        dataIndex: 'rating',
        key: 'rating'
    },
    {
        title: 'Amenities',
        dataIndex: 'amenitiesOffered',
        key: 'amenitiesOffered',
        render: amenities => {
            return amenities.map(amenity => {
                let color = 'magenta'
                if (amenity === 'Curbside Pickup') {
                    color = 'cyan'
                } else if (amenity === 'Delivery') {
                    color = 'red'
                }
                return <Tag color={color} key={amenity}>
                    {amenity}
                </Tag>

            })
        }
    },
    {
        title: 'Actions',
        dataIndex: 'Click for more',
        key: 'Click for more'
    },
];

const data = [
    {
        key: '1',
        num: '1',
        name: 'Walmart Supercenter',
        city: 'Austin, 78704',
        price: '$',
        open: 'yes',
        rating: '2.0',
        amenitiesOffered: ['Masks Required', 'Delivery', 'Curbside Pickup'],
        "Click for more": <ButtonAustin />,
    },
    {
        key: '2',
        num: '2',
        name: 'Walmart Supercenter',
        city: 'Dallas, 75231',
        price: '$',
        open: 'yes',
        rating: '2.0',
        amenitiesOffered: ['Masks Required', 'Delivery'],
        "Click for more": <ButtonDallas />,
    },
    {
        key: '3',
        num: '3',
        name: 'Walmart Supercenter',
        city: 'San Antonio, 78249',
        price: '$',
        open: 'yes',
        rating: '2.5',
        amenitiesOffered: ['Masks Required', 'Delivery'],
        "Click for more": <ButtonSanAntonio />,
    },
];


class StoreList extends React.Component {
    render() {
        return <>
            <div className='store-list-page-header'>
                <PageHeader
                    ghost={false}
                    onBack={() => window.history.back()}
                    title='Walmart'
                    subTitle='Showing all locations for Walmart.'
                >
                <Descriptions size='small' column={3}>
                        <Descriptions.Item label='Number of Instances'>3</Descriptions.Item>
                    </Descriptions>
                    <Descriptions size='small' column={3}>
                        <Descriptions.Item label='Number of Pages'>1</Descriptions.Item>
                </Descriptions>
                </PageHeader>
            </div>
            <div className='store-list-page-header'>
                <Table columns={columns} dataSource={data} />
            </div>
        </>
    }
}
export default StoreList;
