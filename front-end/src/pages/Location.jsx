import React from 'react';
import 'antd/dist/antd.css';
import './styles/location.css'
import { Typography } from 'antd';
import { Image, Descriptions } from 'antd';
import { Table } from 'antd';
import { Link } from 'react-router-dom'
const { Title, Paragraph } = Typography;

const ButtonAustin = () => {
  return (
    <Link className='link' to='/locations/Austin'>Austin Page</Link>
  )
}
const ButtonDallas = () => {
  return (
    <Link className='link' to='/locations/Dallas'>Dallas Page</Link>
  )
}
const ButtonSA = () => {
  return (
    <Link className='link' to='/locations/SanAntonio'>San Antonio Page</Link>
  )
}

const columns = [
  {
    title: 'City',
    dataIndex: 'City',
    key: 'City',
  },
  {
    title: 'State',
    dataIndex: 'State',
    key: 'State',
  },
  {
    title: 'Cost of Living',
    dataIndex: 'CoL',
    key: 'CoL',
  },
  {
    title: 'Connectivity',
    dataIndex: 'Connectivity',
    key: 'Connectivity',
  },
  {
    title: 'Safety',
    dataIndex: 'Safety',
    key: 'Safety',
  },
  {
    title: 'Taxation',
    dataIndex: 'Taxation',
    key: 'Taxation',
  },
  {
    title: 'Latitude',
    dataIndex: 'Latitude',
    key: 'Latitude',
  },
  {
    title: 'Longitude',
    dataIndex: 'Longitude',
    key: 'Longitude',
  },
  {
    title: 'County',
    dataIndex: 'County',
    key: 'County',
  },
  {
    title: 'View',
    dataIndex: 'View',
    key: 'View',
  },
  {
    title: 'Click for more',
    dataIndex: 'Click for more',
    key: 'Click for more',
  },
];

const properties = [
  {
    "City": "Austin",
    "State": "TX",
    "Latitude": 30.292828,
    "Longitude": -97.737368,
    "CoL": 5.35,
    "Connectivity": 1.85,
    "County": "Travis",
    "Safety": 5.06,
    "Taxation": 4.77,
    "View": <AustinPic />,
    "Click for more": <ButtonAustin />
  },
  {
    "City": "Dallas",
    "State": "TX",
    "Latitude": 32.779910,
    "Longitude": -96.802744,
    "CoL": 6.09,
    "Safety": 4.33,
    "Taxation": 4.77,
    "Connectivity": 4.74,
    "County": "Dallas",
    "View": <DallasPic />,
    "Click for more": <ButtonDallas />
  },
  {
    "City": "San Antonio",
    "State": "TX",
    "Latitude": 29.406044,
    "Longitude": -98.507786,
    "CoL": 6.40,
    "Safety": 4.22,
    "Taxation": 4.77,
    "Connectivity": 1.68,
    "County": "Bexar",
    "View": <SanAntonioPic />,
    "Click for more": <ButtonSA />
  }
]


function AustinPic() {
  return (
    <Image
      width={200}
      src="https://fh-sites.imgix.net/sites/4074/2020/04/10195105/Austin-Skyline-Tour-image-1.jpg?auto=compress%2Cformat&fit=crop&crop=faces&w=360&h=240"
    />
  );
}


function SanAntonioPic() {
  return (
    <Image
      width={200}
      src="https://images.fineartamerica.com/images/artworkimages/mediumlarge/3/downtown-san-antonio-skyline-night-bee-creek-photography-tod-and-cynthia.jpg"
    />
  );
}
function DallasPic() {
  return (
    <Image
      width={200}
      src="https://mindfultravelbysara.com/wp-content/uploads/2020/07/Dallas-Skyline-1200x675.jpg"
    />
  );
}
class Location extends React.Component {

  render() {
    return (
      <div>

        <Typography>
          <Title>Cities</Title>
          <Paragraph>
            Listed Are Cities Our Website Currently Supports or Aims to Support
          </Paragraph>
          <Descriptions size='small' column={3}>
                            <Descriptions.Item label='Number of Instances'>3</Descriptions.Item>
                        </Descriptions>
                        <Descriptions size='small' column={3}>
                            <Descriptions.Item label='Number of Pages'>1</Descriptions.Item>
          </Descriptions>
        </Typography>
        <Table dataSource={properties} columns={columns}
        />

      </div>
    )
  }

}
export default Location