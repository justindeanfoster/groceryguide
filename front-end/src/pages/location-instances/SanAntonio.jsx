import React from 'react';
import { Col, Divider, Row, Typography } from 'antd';
import { Image, Button, Tag } from 'antd';
import { Table } from 'antd';
import LifeScoreData from '../static/LifeScore.json';
import { Link } from 'react-router-dom'
import SAData from './static/SAItems.json'
import ItemCard from '../../components/ItemCard'
var items = SAData.items
const { Title, Paragraph } = Typography;

//API Scrubbed
//https://rapidapi.com/wirefreethought/api/geodb-cities/
const properties = [
    {
        "id": 118731,
        "wikiDataId": "Q975",
        "type": "CITY",
        "city": "San Antonio",
        "name": "San Antonio",
        "country": "United States of America",
        "countryCode": "US",
        "region": "Texas",
        "regionCode": "TX",
        "elevationMeters": 198,
        "latitude": 29.425,
        "longitude": -98.493888888,
        "population": 1492510,
        "timezone": "America__Chicago",
        "deleted": false,

    }
]
const Storedata = [
    {
        key: '3',
        num: '3',
        name: 'Walmart Supercenter',
        city: 'San Antonio, 78249',
        price: '$',
        open: 'yes',
        rating: '2.5',
        amenitiesOffered: ['Masks Required', 'Delivery'],

    },
]

const StoreCol = [
    {
        title: 'Store',
        dataIndex: 'num',
        key: 'num',
        render: num => {
            let url = `/stores/walmart/${num}`
            return <Link to={url}>
                <Button type='primary'> Go to store </Button>
            </Link>
        }
    },
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name'
    },
    {
        title: 'City',
        dataIndex: 'city',
        key: 'city',
    },
    {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        // render: _ => {
        //     return `$${this.props.price}`
        // }
    },
    {
        title: 'Open',
        dataIndex: 'open',
        key: 'open'
    },
    {
        title: 'Rating',
        dataIndex: 'rating',
        key: 'rating'
    },
    {
        title: 'Amenities',
        dataIndex: 'amenitiesOffered',
        key: 'amenitiesOffered',
        render: amenities => {
            return amenities.map(amenity => {
                let color = 'magenta'
                if (amenity === 'Curbside Pickup') {
                    color = 'cyan'
                } else if (amenity === 'Delivery') {
                    color = 'red'
                }
                return <Tag color={color} key={amenity}>
                    {amenity}
                </Tag>

            })
        }
    },

];

const columns = [
    {
        title: 'Population',
        dataIndex: 'population',
        key: 'population',
    },
    {
        title: 'Elevation',
        dataIndex: 'elevationMeters',
        key: 'elevationMeters',
    },
    {
        title: 'Time zone',
        dataIndex: 'timezone',
        key: 'timezone',
    },
    {
        title: 'Region',
        dataIndex: 'regionCode',
        key: 'regionCode',
    },

];

const columnsScore = [
    {
        title: 'Feature',
        dataIndex: 'name',
        key: 'feature',
    },
    {
        title: 'Score out of 10',
        dataIndex: 'score_out_of_10',
        key: 'score'
    },
];

function ImageDemo() {
    return (
        <div style={{ textAlign: 'center' }}>
            <Image
                width={800}
                src="https://www.thealamo.org/fileadmin/_processed_/3/9/csm_a27084a0214_f71edad0d0.jpg"
            />
        </div>
    );
}

function SAImage() {
    return (
        <div style={{ textAlign: 'center' }}>
            <Image
                width={400}
                src="https://gisgeography.com/wp-content/uploads/2020/06/San-Antonio-Map-Texas-1265x1262.jpg"
            />
        </div>
    );
}



const SanAntonio = () => {

    const itemCards = items.map(prop => {
        prop.isStore = false
        return <ItemCard {...prop} />
    })

    return <div className='page-header-wrapper'>
        <Typography>
            <Title style={{ textAlign: 'center' }}>San Antonio, Texas</Title>
            <ImageDemo />
            <br />
            <br />
            <div>
                <Table dataSource={properties} columns={columns} />
            </div>

            <Paragraph>
                <Divider>
                    <h3>Life Score of San Antonio</h3>
                </Divider>
                <br />
                <div>
                    {/* API from Teleport
                https://api.teleport.org/api/cities/?search=San%20Antonio&embed=city%3Asearch-results%2Fcity%3Aitem%2Fcity%3Aurban_area%2Fua%3Ascores */}
                    <Table dataSource={LifeScoreData.SanAntonio} columns={columnsScore} pagination={{ pageSize: 50 }} scroll={{ y: 400 }} />
                </div>
            </Paragraph>
            <Paragraph>
            <Divider>
                <h3> Recommended Stores </h3>
            </Divider>
          </Paragraph>
            <Table columns={StoreCol} dataSource={Storedata} />
            <Paragraph>

                <Divider> <h3> Items Offered </h3> </Divider>
                <div className='page-header-wrapper'>
                    <Row gutter={20}>
                        <Col span={8}>
                            {itemCards[0]}
                        </Col>
                        <Col span={8}>
                            {itemCards[1]}
                        </Col>
                        <Col span={8}>
                            {itemCards[2]}
                        </Col>
                    </Row>
                </div>
            </Paragraph>

            <Paragraph style={{ textAlign: 'center' }}>Nearby Stores in San Antonio</Paragraph>

            <SAImage />
        </Typography>
    </div>
}

export default SanAntonio;