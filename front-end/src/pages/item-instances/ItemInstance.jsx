import React from 'react';
import 'antd/dist/antd.css';
import './itemInstance.css'
import { Card, Row, Col, PageHeader, Divider, Typography, Button } from 'antd'
import { Link } from 'react-router-dom'
import { Table, Tag } from 'antd';

const { Meta } = Card
const { Title } = Typography


const colorMappingCategories = {
    'Organic': 'green',
    'USDA Organic': 'green',
    'Dairy': 'white',
    'processed': "dark grey"
}

const ButtonAustin = () => {
    return (
        <Link to='/locations/Austin'>
            <Button type='secondary'> See city </Button>
        </Link>
    )
}

const ButtonDallas = () => {
    return (
        <Link to='/locations/Dallas'>
            <Button type='secondary'> See city </Button>
        </Link>
    )
}

const ButtonSanAntonio = () => {
    return (
        <Link to='/locations/SanAntonio'>
            <Button type='secondary'> See city </Button>
        </Link>
    )
}


class ItemInstance extends React.Component {
    render() {

        const columns = [
            {
                title: 'Store',
                dataIndex: 'num',
                key: 'num',
                render: num => {
                    let url = `/stores/walmart/${num}`
                    return <Link to={url}>
                        <Button type='primary'> Go to store </Button>
                    </Link>
                }
            },
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'name'
            },
            {
                title: 'City',
                dataIndex: 'city',
                key: 'city',
            },
            {
                title: 'Price',
                dataIndex: 'price',
                key: 'price',
                render: _ => {
                    return `$${this.props.price}`
                }
            },
            {
                title: 'Open',
                dataIndex: 'open',
                key: 'open'
            },
            {
                title: 'Rating',
                dataIndex: 'rating',
                key: 'rating'
            },
            {
                title: 'Amenities',
                dataIndex: 'amenitiesOffered',
                key: 'amenitiesOffered',
                render: amenities => {
                    return amenities.map(amenity => {
                        let color = 'magenta'
                        if (amenity === 'Curbside Pickup') {
                            color = 'cyan'
                        } else if (amenity === 'Delivery') {
                            color = 'red'
                        }
                        return <Tag color={color} key={amenity}>
                            {amenity}
                        </Tag>
        
                    })
                }
            },
            {
                title: 'Actions',
                dataIndex: 'Click for more',
                key: 'Click for more'
            },
        ];
        
        const data = [
            {
                key: '1',
                num: '1',
                name: 'Walmart Supercenter',
                city: 'Austin, 78704',
                price: '$',
                open: 'yes',
                rating: '2.0',
                amenitiesOffered: ['Masks Required', 'Delivery', 'Curbside Pickup'],
                "Click for more": <ButtonAustin />,
            },
            {
                key: '2',
                num: '2',
                name: 'Walmart Supercenter',
                city: 'Dallas, 75231',
                price: '$',
                open: 'yes',
                rating: '2.0',
                amenitiesOffered: ['Masks Required', 'Delivery'],
                "Click for more": <ButtonDallas />,
            },
            {
                key: '3',
                num: '3',
                name: 'Walmart Supercenter',
                city: 'San Antonio, 78249',
                price: '$',
                open: 'yes',
                rating: '2.5',
                amenitiesOffered: ['Masks Required', 'Delivery'],
                "Click for more": <ButtonSanAntonio />,
            },
        ];

        return <>
        <div className='item-list-page-header'>
            <PageHeader
            ghost={false}
            onBack={() => window.history.back()}
            title={ this.props.itemName }
            subTitle='Overview'
            />
        </div>
        <Divider>
                <Typography>
                    <Title level={ 3 }> General Info: </Title>
                </Typography>
        </Divider>
        <div style={{ padding: '30px' }}>
            <Row gutter={32} justify='center'>
            <Col span={12}>
                <Card 
                bordered={false}
                cover={ 
                    <img
                    src={ this.props.imageLink }
                    className='cropped-image'
                    alt= { this.props.itemName }
                    />
                }
                className='item-info-card'
                >
                    <Meta 
                    title={ <div> {this.props.itemName} </div>}
                    description={
                        <div>
                            <b> Bar Code: </b> <p> { this.props.barCode } </p>
                            <b> Attributes: </b> <br/>
                            <div>
                                {
                                    this.props.categories.map((category) => {
                                        let color = 'gray'
                                        if (category in colorMappingCategories) {
                                            color = colorMappingCategories[category]
                                        }
                                        return <Tag color={color}> {category} </Tag>
                                    })
                                }
                            </div>
                        </div>
                    }
                    ></Meta>
                </Card>
            </Col>
            <Col span={6}>
                <Card 
                title='Nutrition Facts' 
                bordered={false}
                className='item-info-card'
                cover={ 
                    <img
                    src={ this.props.nutritionFacts }
                    className='cropped-image'
                    alt='Nutrition Facts'
                    />
                    }
                >
                    <Meta 
                    title={ <div> Related Nutrition Facts </div>}
                    description={
                        <div>
                            <b> Nutri-Score: </b> <p> { this.props.nutriScore } </p>
                            <b> Nova-Score: </b> <p> { this.props.novaScore } </p>
                            <b> Serving Size: </b> <p> { this.props.servingSize } </p>
                        </div>
                    }
                    ></Meta>
                </Card>
            </Col>
            </Row>

            <Divider>
                <Typography>
                    <Title level={ 3 }> Available in: </Title>
                </Typography>
            </Divider>
            <div className='page-header-wrapper'>
                <div className='store-list-page-header'>
                    <Table columns={columns} dataSource={data} />
                </div>
            </div>
        </div>
    </>
    }
}
export default ItemInstance
