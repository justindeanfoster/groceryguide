import React from 'react'
import 'antd/dist/antd.css'
import './styles/card.css'
import { Card, Popover } from 'antd'
import { Link } from 'react-router-dom'
import { BarsOutlined } from '@ant-design/icons'

const { Meta } = Card;
class ItemCard extends React.Component {
    render() {

        const linkTo = `/items/${this.props.id}`
        return <>
            <Card
            cover={
                <img
                    alt='Item'
                    src= { this.props.imageLink }
                    className='cropped-image'
                />
            }
            actions={[

                <Popover content='More info on item.'>
                    <Link to={ linkTo }>
                        <BarsOutlined key='more' />
                    </Link>
                </Popover>
            ]}
            className='itemcard'
            >
                <Meta
                    title= { this.props.itemName }
                    description= { 
                        <div>
                        {
                            this.props.isStore ? 
                            <div>
                                <b className='bolded'> Price: </b> ${ this.props.price }
                                <br/>
                            </div> : <div/>
                        }
                        <b>Nova Score: </b> {
                            this.props.novaScore ? this.props.novaScore : 'N/A'
                        } <br/>

                        <b>Nutriscore: </b> {
                            this.props.nutriScore ? this.props.nutriScore : 'N/A'
                        } <br/>

                        <b>Serving Size: </b> {
                            this.props.servingSize ? this.props.servingSize : 'N/A'
                        } <br/>

                        <b>Calories per Serving: </b> {
                            this.props.calories ? `${this.props.calories} kcal` : 'N/A'
                        } <br/>

                        <b>Labels: </b> {
                            this.props.labels.length > 0 ? `${this.props.labels.join(', ')}` : 'None'
                        } <br/>
                        
                        <b>Brands: </b> {
                            this.props.brands.length > 0 ? `${this.props.brands.join(', ')}` : 'Unavailable'
                        } <br/>
                        </div>
                    }
                />
            </Card>
        </>
    }
}
export default ItemCard
