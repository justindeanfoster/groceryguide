import React from "react";
import GoogleMapReact from 'google-map-react';
import './styles/mapcontainer.css'
import { Icon } from '@iconify/react'
import locationIcon from '@iconify/icons-mdi/map-marker'

const LocationPin = ({ text }) => (
    <div className="pin">
      <Icon icon={locationIcon} className="pin-icon" />
      <p className="pin-text">{text}</p>
    </div>
)

const mapKey = 'AIzaSyCZVCIofvFoGl2DA2WdTcDMrwpi21VdcLk'

class MapContainer extends React.Component {
    render() {
        return (
            <div style={{ height: '300px', width: '300px' }}>
              <GoogleMapReact
                bootstrapURLKeys={{ key: mapKey }}
                defaultCenter={ 
                    {
                        lat: this.props.latitude,
                        lng: this.props.longitude
                    }
                }
                defaultZoom={ 15 }
              >
              <LocationPin lat={ this.props.latitude } lng={ this.props.longitude } />
              </GoogleMapReact>
            </div>
          );
    }
}
export default MapContainer
