import React from 'react';
import 'antd/dist/antd.css';
import './styles/card.css'
import { Card, Popover } from 'antd'
import { Link } from 'react-router-dom'
import { EnvironmentOutlined } from '@ant-design/icons';


const { Meta } = Card;
class StoreCard extends React.Component {
    render() {
        return <>
            <Card
            cover={
                <img
                    alt='Store'
                    src= {this.props.storeImage}
                    className='storecardImage'
                />
            }
            actions={[
                <Popover content='Look for available locations.'>
                    <Link to={ this.props.linkTo }>
                        <EnvironmentOutlined key='goto'/>
                    </Link>
                </Popover>,
            ]}
            className='storecard'
            >
            <Meta
                title= { <div style={{ textAlign: 'center' }}> { this.props.storeName } </div> }
            />
        </Card>
    </>
    }
}
export default StoreCard
