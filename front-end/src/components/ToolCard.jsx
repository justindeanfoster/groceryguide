import React from 'react';
import 'antd/dist/antd.css';
import './styles/card.css'
import { Card } from 'antd'


const { Meta } = Card;
class ToolCard extends React.Component {
    render() {
        return <>
            <Card
            cover={
                <img
                    alt='Tool'
                    src= { this.props.image }
                    className='croppedImage'
                />
            }
            className='teamcard'
            >
            <Meta
                title= { this.props.name }
                description= {
                    <div>
                        <b>Link: </b> { this.props.link } <br/>

                        { this.props.description }
                    </div>
                }
            />
        </Card>
    </>
    }
}
export default ToolCard
